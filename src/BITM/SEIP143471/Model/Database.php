<?php

namespace App\Model;
use PDO;
use PDOException;

class Database
{
    public $conn;
    public $host="localhost";
    public $dbname="atomic_project_b35";
    public $username="root";
    public $password="";

    public function __construct()
    {
        try
        {
            $this->conn= new PDO("mysql:host=$this->host;dbname=$this->dbname",$this->username,$this->password);
            //echo "Connected Successfully!";
        }
        catch(PDOException $e){
            echo $e->getMessage();
        }
    }
}

$objDatabase = new Database();